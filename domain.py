#!/usr/bin/env python3
#
# Copyright 2019 Morten Jakobsen. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.
#
# -*- coding: utf-8 -*-

def column(length, text):
    whitespace = length - len(text)
    return text+whitespace*" "

import fileinput
import argparse
import configparser
import json

parser = argparse.ArgumentParser(description="Ansible domain list updater")
parser.add_argument('-a', '--adddomain', type=str, help="Domain to add", default=None)
parser.add_argument('-d', '--db', type=str, help="Database name", default=None)
parser.add_argument('-p', '--password', type=str, help="Database password", default=None)
parser.add_argument('-r', '--removedomain', type=str, help="Domain to remove", default=None)
parser.add_argument('-c', '--clearremoved', help="Clear delete domain variable", action='store_true')
args = parser.parse_args()
config = configparser.ConfigParser()
config.read("inventory.ini")
domainList = json.loads(config['all:vars']['domains'].strip())
removeDomainList = json.loads(config['all:vars']['domains_reset'].strip())

if args.adddomain or args.removedomain or args.clearremoved:
    save = False

    if args.adddomain:
        allSet = True
        if args.password is None:
            print("Missing --password argument")
            allSet = False
        if args.db is None:
            print("Missing --db argument")
            allSet = False

        if allSet == True:
            for i,d in enumerate(domainList):
                if d['domain'] == args.adddomain:
                    print("Domain exists, replacing config")
                    del[domainList[i]]

            for i,d in enumerate(removeDomainList):
                if d['domain'] == args.adddomain:
                    print("Domain exists, replacing config")
                    del[removeDomainList[i]]
            
            domainList.append({
                'domain': args.adddomain,
                'db': args.db,
                'pass': args.password
            })
            save = True

    if args.removedomain:
        for i,d in enumerate(domainList):
            if d['domain'] == args.removedomain:
                removeDomainList.append({
                    'domain': d['domain'],
                    'db': d['db'],
                    'pass': d['pass']
                })
                del[domainList[i]]
                print("Domain deleted")
        save = True

    if args.clearremoved:
        removeDomainList = []
        save = True

    if save == True:
        config['all:vars']['domains'] = json.dumps(domainList)
        config['all:vars']['domains_reset'] = json.dumps(removeDomainList)
        with open("inventory.ini", 'w') as configfile:
            config.write(configfile)

        with fileinput.FileInput("inventory.ini", inplace=True, backup='.bak') as file:
            for line in file:
                print(line.replace(" = ", "="), end='')

else:
    c1 = 20
    c2 = 20
    c3 = 40
    print("Configured domains")
    print("{}{}{}".format(column(c1, 'Domain'), column(c2, 'DB'), column(c3, 'Password')))
    print((c1+c2+c3)*"=")
    for i, d in enumerate(domainList):
        print("{}{}{}".format(column(c1, d['domain']), column(c2, d['db']), column(c3, d['pass'])))

    print("\n\nDeleted domains")
    print("{}{}{}".format(column(c1, 'Domain'), column(c2, 'DB'), column(c3, 'Password')))
    print((c1+c2+c3)*"=")
    for i, d in enumerate(removeDomainList):
        print("{}{}{}".format(column(c1, d['domain']), column(c2, d['db']), column(c3, d['pass'])))

    print("\n\nRemember to execute ansible to deploy the update")