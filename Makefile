all:
	git pull
	ansible-playbook site.yml

fileserver:
	git pull
	ansible-playbook fileserver.yml

mariadb:
	git pull
	ansible-playbook mariadbserver.yml

web:
	git pull
	ansible-playbook webserver.yml

ansible:
	git pull
	ansible-playbook ansible.yml